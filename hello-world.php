<html>
<head>
  <meta charset="UTF-8" />
</head>

<body><pre><?php
// A simple web site in Cloud9 that runs through Apache
// Press the 'Run' button on the top to start the web server,
// then click the URL that is emitted to the Output tab of the console

// echo 'Hello world from Cloud9!';

$url = "/home/ubuntu/workspace/altre/cryptonloidcosplay/all.json";

$contents = file_get_contents($url);
// $contents = utf8_encode($contents);
$results = json_decode($contents, true); 


$unset_names= array(
  'can_like',
  'can_reblog',
  'can_reply',
  'can_send_in_message',
  'display_avatar',
  'format',
  'note_count',
  'reblog_key',
  'recommended_color',
  'recommended_source',
  'source_url',
  'state',
  'reblog',
  'reblogged_from_can_message',
  'reblogged_from_id',
  'reblogged_from_name',
  'reblogged_from_title',
  'reblogged_from_url',
  'reblogged_from_uuid',
  'reblogged_root_can_message',
  'reblogged_root_id',
  'reblogged_root_name',
  'reblogged_root_title',
  'reblogged_root_url',
  'reblogged_root_uuid',
  'trail'
);



foreach ($results as $key => $value) {

  $results[$key]['photos_num'] = count($results[$key]['photos']);
  foreach ($unset_names as $value) {
    unset($results[$key][$value]);
  }
  $results[$key]['ext'] = substr($results[$key]['photos'][0]['original_size']['url'], -3);
  $x = $results[$key]['tags'];
  $results[$key]['l_tag'] = implode(', ', $x);
  
  unset($results[$key]['photos']);
  // foreach ($results[$key]['photos'] as $i_key => $i_value) {
  //   $results[$key]['photos'][$i_key] = true;
  // }
  for ($i=0; $i < $results[$key]['photos_num']; $i++) { 
    $results[$key]['img'][] = $results[$key]['id'] . "_o$i.".$results[$key]['ext'];
  }
  echo $results[$key]['l_tag'] . "\n";
}

// print_r($results);
?>
</pre>
</body>
</html>